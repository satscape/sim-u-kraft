package info.satscape.simukraft.common;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;

public class ItemGranulesGold extends Item {
	private Icon icons[];
	
	public ItemGranulesGold(int par1) {
		super(par1);
	    maxStackSize = 64;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister iconRegister) {
		icons=new Icon[1];
		icons[0] = iconRegister.registerIcon("satscapesimukraft:granulesGold");
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public Icon getIconFromDamage(int par1) {
		return icons[0];
	}

	@Override
	public String getItemDisplayName(ItemStack par1ItemStack) {
		return "Gold granules";
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack par1ItemStack,
			EntityPlayer par2EntityPlayer, List par3List, boolean par4) {

		par3List.add("Place into furnace to make Gold ingots");
		super.addInformation(par1ItemStack, par2EntityPlayer,par3List , par4);
	}

	@Override
	public Icon getIcon(ItemStack stack, int pass) {
		return icons[0];
	}
	
}
